package com.ucuenca.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import com.ucuenca.repository.UserInfoRepo;

import java.util.List;
import java.util.Set;

@Configuration
public class UserInfoDAO {
    @Autowired
    private UserInfoRepo userInfoRepo;

    public Set<String> getUserRole(String userName) {
        return userInfoRepo.getUserRoleBY(userName);
    }
}
