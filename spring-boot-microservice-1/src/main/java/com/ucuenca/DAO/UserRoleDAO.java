package com.ucuenca.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import com.ucuenca.repository.UserRoleRepo;

import java.util.List;

@Configuration
public class UserRoleDAO {
    @Autowired
    private UserRoleRepo userRoleRepo;

    public List<String> getUserRolePermission(String userRole) {
        return userRoleRepo.getUserRolePermissionBY(userRole);
    }
}
