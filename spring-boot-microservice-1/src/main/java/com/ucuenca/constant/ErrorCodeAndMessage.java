package com.ucuenca.constant;

public enum ErrorCodeAndMessage {

    JWT_TOKEN_NOT_VALID("ER-1004", "User token is not valid.");
    private String errorCode;
    private String message;

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    ErrorCodeAndMessage(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
    }
