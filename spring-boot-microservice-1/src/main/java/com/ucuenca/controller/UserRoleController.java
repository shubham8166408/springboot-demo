package com.ucuenca.controller;


import com.ucuenca.DAO.UserInfoDAO;
import com.ucuenca.DAO.UserRoleDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ucuenca.service.impl.JwtServiceImpl;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/v1")
public class UserRoleController {

    @Autowired
    private JwtServiceImpl jwtService;

    @Autowired
    UserInfoDAO userInfoDAO;

    @Autowired
    UserRoleDAO userRoleDAO;

    @GetMapping(value="/get")
    public Set<String> getUserRole(@RequestHeader("X-Auth-Token") String authToken){
      String userName = jwtService.verifyUserToken(authToken);
      Set<String> roleList =  userInfoDAO.getUserRole(userName);
      Set<String> rolePermission=new HashSet<>();
      for(String role:roleList){
          rolePermission.addAll(userRoleDAO.getUserRolePermission(role));
      }
      return rolePermission;
    }

}
