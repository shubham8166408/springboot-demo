package com.ucuenca.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@RequiredArgsConstructor
@Entity
@Data
@Table(name="user_info")
public class UserInfo {

    @Id
    @Column(name="id",unique =true)
    private Integer id;

    @Column(name="user_id")
    private Integer user_id;

    @Column(name="Username",length =100)
    private String userName;

    @Column(name="user_role",length =100)
    private String user_role;
}
