package com.ucuenca.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@RequiredArgsConstructor
@Entity
@Data
@Table(name="user_role")
public class UserRole {

    @Id
    @Column(name="id" ,unique = true)
    private Integer id;

    @Column(name="user_role",length = 100)
    private String user_role;

    @Column(name="user_role_permission",length =100)
    private String user_role_permission;

}
