package com.ucuenca.exception;
import com.ucuenca.constant.ErrorCodeAndMessage;
import lombok.Data;

@Data
public class JWTTokenException extends OnBoardException {

    private static final long serialVersionUID = 1L;

    private String errorCode;
    private String errorMessage;

    public JWTTokenException(ErrorCodeAndMessage errorCodeAndMessage) {
        super(errorCodeAndMessage.getMessage());
        this.errorCode = errorCodeAndMessage.getErrorCode();
        this.errorMessage = errorCodeAndMessage.getMessage();
    }

    public JWTTokenException(ErrorCodeAndMessage errorCodeAndMessage, String message) {
        super(message);
        this.errorCode = errorCodeAndMessage.getErrorCode();
        this.errorMessage = errorCodeAndMessage.getMessage();

    }

    public JWTTokenException(ErrorCodeAndMessage errorCodeAndMessage, String message, Throwable cause) {
        super(message,cause);
        this.errorCode = errorCodeAndMessage.getErrorCode();
        this.errorMessage = errorCodeAndMessage.getMessage();

    }

    public JWTTokenException(ErrorCodeAndMessage errorCodeAndMessage, Throwable cause) {
        super(errorCodeAndMessage.getMessage(),cause);
        this.errorCode = errorCodeAndMessage.getErrorCode();
        this.errorMessage = errorCodeAndMessage.getMessage();

    }
    public JWTTokenException() {
        this(ErrorCodeAndMessage.JWT_TOKEN_NOT_VALID);
    }

}
