package com.ucuenca.exception;

public class OnBoardException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    public OnBoardException() {

    }

    public OnBoardException(Throwable cause) {
        initCause(cause);
    }

    public OnBoardException(String message) {
        super(message);
    }

    public OnBoardException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}