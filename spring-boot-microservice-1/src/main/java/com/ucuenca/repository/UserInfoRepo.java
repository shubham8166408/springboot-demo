package com.ucuenca.repository;

import com.ucuenca.entity.UserInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface UserInfoRepo extends CrudRepository<UserInfo,Integer> {
    @Query("select t.user_role from UserInfo t where t.userName=:userName")
    Set<String> getUserRoleBY(@Param("userName") String userName);
}
