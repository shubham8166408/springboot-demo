package com.ucuenca.repository;

import com.ucuenca.entity.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepo extends CrudRepository<UserRole, Integer> {
    @Query("select t.user_role_permission from UserRole t where t.user_role=:user_role")
    List<String> getUserRolePermissionBY(@Param("user_role") String userRole);
}
