package com.ucuenca.service.impl;

import com.ucuenca.constant.Constants;
import com.ucuenca.exception.JWTTokenException;
import com.ucuenca.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class JwtServiceImpl implements JwtService {

    @Value("${user.session.expiry.date}")
    private Integer noOfDays;

    @Value("${user.token.secret.sign.key}")
    private String signingKey;

    public String getUserSessionToken(String userName) {
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(signingKey);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
        Map<String,Object> claims = new HashMap<String, Object>();
        claims.put(Constants.USERNAME,userName);

        return Jwts.builder()
                .setIssuer(Constants.ON_BOARD)
                .setIssuedAt(Date.from(Instant.ofEpochSecond(LocalDateTime.now().atZone(ZoneId.of("Asia/Kolkata")).toEpochSecond())))
                .setExpiration(Date.from(Instant.ofEpochSecond(LocalDateTime.now().plusDays(noOfDays).atZone(ZoneId.of("Asia/Kolkata")).toEpochSecond())))
                .claim(Constants.USERNAME,userName)
                .signWith(signatureAlgorithm,signingKey).compact();
    }

    @Override
    public String verifyUserToken(String jwtUserToken) {
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(signingKey))
                .parseClaimsJws(jwtUserToken).getBody();
        if(!Constants.ON_BOARD.equals(claims.getIssuer())){
            throw new JWTTokenException();
        }
        if(Objects.isNull(claims.get(Constants.USERNAME))){
            throw new JWTTokenException();
        }
        return ((String) claims.get(Constants.USERNAME));
    }

}

