package com.ucuenca.DAO;


import com.ucuenca.repository.UserCredentialsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserCredentialDAO {

    @Autowired
    private UserCredentialsRepo userCredentialsRepo;

    public String getPasswordBy(String userName) {
        return userCredentialsRepo.getPasswordBy(userName);
    }
}
