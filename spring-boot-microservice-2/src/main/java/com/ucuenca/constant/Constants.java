package com.ucuenca.constant;

public class Constants {
    public static final String USERNAME = "username";
    public static final String ON_BOARD = "onboard";
    public static final String SUCCESSFULLY_PASSWORD_CHANGE = "Successfully changed password.";
}
