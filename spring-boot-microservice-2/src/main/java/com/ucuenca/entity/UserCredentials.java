package com.ucuenca.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@RequiredArgsConstructor
@Entity
@Data
@Table(name="usercredentials")
public class UserCredentials {

    @Id
    @Column(name="Id",unique = true)
    private Integer id;

    @Column(name="Username",length = 100)
    private String userName;

    @Column(name="Password",length = 100)
    private String password;
}
