package com.ucuenca.filter;

import com.ucuenca.DAO.UserCredentialDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class UserAuthentication implements Filter {

    @Autowired
    private UserCredentialDAO userCredentialDAO;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        String userName = httpRequest.getHeader("username");
        String password = httpRequest.getHeader("password");
        String fetchedPassword = userCredentialDAO.getPasswordBy(userName);
        if(!password.equals(fetchedPassword)){
            throw new RuntimeException("Invalid username or password.");
        }
        //doFilter
        filterChain.doFilter(httpRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
