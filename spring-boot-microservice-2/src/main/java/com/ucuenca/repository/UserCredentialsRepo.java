package com.ucuenca.repository;



import com.ucuenca.entity.UserCredentials;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserCredentialsRepo extends CrudRepository<UserCredentials, Integer> {
    @Query("select t.password from UserCredentials t where t.userName=:userName")
    String getPasswordBy(@Param("userName") String userName);
}
