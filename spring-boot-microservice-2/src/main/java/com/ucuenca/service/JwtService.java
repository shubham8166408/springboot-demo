package com.ucuenca.service;

public interface JwtService {
    String getUserSessionToken(String customerId);

    String verifyUserToken(String jwtUserToken);
}