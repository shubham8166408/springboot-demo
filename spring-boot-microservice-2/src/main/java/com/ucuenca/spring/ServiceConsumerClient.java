package com.ucuenca.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Set;

@Configuration
@EnableFeignClients
@EnableDiscoveryClient
public class ServiceConsumerClient {

    @Autowired
    private TheClient theClient;

    @FeignClient(name = "GreetingMicroservice")
    interface TheClient {

        @RequestMapping(path = "/api/v1/get", method = RequestMethod.GET)
        @ResponseBody
        public Set<String> getUserRole(@RequestHeader("X-Auth-Token") String jwtToken);
    }

    public Set<String> getUserRole(String jwtToken) {
        return theClient.getUserRole(jwtToken);
    }
}
