package com.ucuenca.spring;

import com.ucuenca.service.impl.JwtServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.Set;

@RestController
@RequestMapping("/api/v2")
public class UserController {

    @Autowired
    private JwtServiceImpl jwtService;

    @Autowired
    ServiceConsumerClient feignClientClass;

    @GetMapping(value = "/login")
    public Set<String> login(@RequestHeader String username, @RequestHeader String password) {
        String jwtToken = jwtService.getUserSessionToken(username);
        return feignClientClass.getUserRole(jwtToken);
    }
}
